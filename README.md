# SolutisRobotArena

<h2>Como o Atom funciona</h2>

O robô inicia a partir do método run, onde repetidamente ele gira o canhão para a esquerda em 10 graus, e dependendo da condição ser verdadeira, ele se movimenta para frente fazendo curvas de até 100 graus aleatoriamente.

Quando o scanner detecta um robô a condição de movimento muda para false e o robô para de se mover e foca em atirar no inimigo, a ideia é atirar com mais força se a energia estiver mais alta e o inimigo não estiver muito distante, caso a quantidade de tiros errados ultrapasse 3, o robô volta a se movimentar em busca de um novo alvo.

<h2>Sobre o Roboce</h2>
Usar o robocode é uma ótima experiência para testar a programação de forma visual, dessa forma é possível enxergar como os métodos e atributos afetam o robô e experimentar várias estratégias com o robô que está programando
