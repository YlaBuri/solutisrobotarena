package meuRobo;
import robocode.*;
import java.awt.Color;
import robocode.util.Utils;

/**
 * Atom - a robot by (Yla)
 */
public class Atom extends AdvancedRobot
{
	/**
	 * run: Atom's default behavior
	 */
	String mirarRobo= null;
	boolean andar = true;
	boolean girarArma = true;
	int contarErros=0;		

	public void run() {
			
	
			setBodyColor(Color.lightGray);
			setGunColor(Color.darkGray);
			setRadarColor(Color.blue);
			setScanColor(Color.lightGray);		
	
			
			while(true){
			
				if(andar == true) {
					
					double girar = 1 + (int) (Math.random() * 100);
					setAhead(100);
					if(girar % 2 ==0)
	            		setTurnRight(girar);
					else
						setTurnLeft(girar);     
					 
				}
				if(girarArma== true)
					setTurnGunRight(10);
				execute();
			}		
	}
	
	public void onScannedRobot(ScannedRobotEvent e) {
			
			if(mirarRobo == null)
				mirarRobo = e.getName();
				 
			atirar(e);	
	}
	
	public void atirar(ScannedRobotEvent e){
		//girarArma =false;
		if(contarErros < 3){
			andar=false;
			setTurnRight(e.getBearing()+90) ;
			double absoluteBearing=e.getBearingRadians()+getHeadingRadians();
			double gunTurn = absoluteBearing - getGunHeadingRadians() ;
			
			double extraTurn = Math.atan(36.0/e.getDistance())*(gunTurn >= 0 ? 1 : -1);
			setTurnGunRightRadians(Utils.normalRelativeAngle(gunTurn+extraTurn));	
			
			if(getEnergy()>50 && e.getDistance() < 350) {
				setFire(3);
			}else {
				setFire(1);
			}
		}else{
			contarErros =0;
			mirarRobo = null;
			andar=true;
		}
	}
	
	
	public void onHitWall(HitWallEvent e) {
		setBack(20);
		turnRight(180);
	}	
	
	public void onBulletHit(BulletHitEvent e){
	contarErros = 0;
	}
	
	
	public void onBulletMissed(BulletMissedEvent evento) {
		contarErros++;
		if(contarErros >= 3){
			andar = true;
			mirarRobo= null;
		}
	}
	

}
